# Assignment 2.1 - node.js project
A NODEjs project that uses express.

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [About](#about)
- [Contributors](#contributers)

## Requirements

Requires node.js. 

## Setup
    $ npm install
    $ npm run dev
    $ localhost:8080
    $ CTRL+C to terminate


## About
Returns an index.html file containing a header, an image, a list and a link to bing.com. 


## Contributors
[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)
